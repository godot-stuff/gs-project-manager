# Release Notes
## v0.1.x
### Title

```
```

For a full list of changes, please see the projects [History](HISTORY.md) file.

Much more to come, check the roadmap, and let us know if there is something else you would like to be added by sending us a note on Facebook or creating an [Issue](https://gitlab.com/godot-stuff/gs-project-manager/-/issues) on the project page.

Enjoy the new release!
