
import unittest   # The test framework
import gspm.utils.godot_download as godot_download

class TestDownloadTux(unittest.TestCase):



	# test all major stable windows versions

	def test_v1_0_win(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "windows",
			version = "1.0",),
			"https://downloads.tuxfamily.org/godotengine/1.0/Godot_v1.0_stable_win64.exe.zip")

	def test_v1_1_win(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "windows",
			version = "1.1"),
			"https://downloads.tuxfamily.org/godotengine/1.1/Godot_v1.1_stable_win64.exe.zip")

	def test_v2_0_win(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "windows",
			version = "2.0"),
			"https://downloads.tuxfamily.org/godotengine/2.0/Godot_v2.0_stable_win64.exe.zip")

	def test_v2_1_win(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "windows",
			version = "2.1"),
			"https://downloads.tuxfamily.org/godotengine/2.1/Godot_v2.1-stable_win64.exe.zip")

	def test_v3_0_win(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "windows",
			version = "3.0"),
			"https://downloads.tuxfamily.org/godotengine/3.0/Godot_v3.0-stable_win64.exe.zip")

	def test_v3_1_win(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "windows",
			version = "3.1"),
			"https://downloads.tuxfamily.org/godotengine/3.1/Godot_v3.1-stable_win64.exe.zip")

	def test_v3_2_win(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "windows",
			version = "3.2"),
			"https://downloads.tuxfamily.org/godotengine/3.2/Godot_v3.2-stable_win64.exe.zip")

	def test_v3_3_win(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "windows",
			version = "3.3"),
			"https://downloads.tuxfamily.org/godotengine/3.3/Godot_v3.3-stable_win64.exe.zip")

	def test_v3_4_win(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "windows",
			version = "3.4"),
			"https://downloads.tuxfamily.org/godotengine/3.4/Godot_v3.4-stable_win64.exe.zip")

	def test_v3_5_win(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "windows",
			version = "3.5"),
			"https://downloads.tuxfamily.org/godotengine/3.5/Godot_v3.5-stable_win64.exe.zip")

	def test_v4_0_win(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "windows",
			version = "4.0"),
			"https://downloads.tuxfamily.org/godotengine/4.0/Godot_v4.0-stable_win64.exe.zip")

	def test_v4_1_win(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "windows",
			version = "4.1"),
			"https://downloads.tuxfamily.org/godotengine/4.1/Godot_v4.1-stable_win64.exe.zip")

	# test all major stable macos versions

	def test_v1_0_mac(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "darwin",
			version = "1.0"),
			"https://downloads.tuxfamily.org/godotengine/1.0/Godot_v1.0_stable_osx32.zip")

	def test_v1_1_mac(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "darwin",
			version = "1.1"),
			"https://downloads.tuxfamily.org/godotengine/1.1/Godot_v1.1_stable_osx32.zip")

	def test_v2_0_mac(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "darwin",
			version = "2.0"),
			"https://downloads.tuxfamily.org/godotengine/2.0/Godot_v2.0_stable_osx32.zip")

	def test_v2_1_mac(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "darwin",
			version = "2.1"),
			"https://downloads.tuxfamily.org/godotengine/2.1/Godot_v2.1-stable_osx.fat.zip")

	def test_v3_0_mac(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "darwin",
			version = "3.0"),
			"https://downloads.tuxfamily.org/godotengine/3.0/Godot_v3.0-stable_osx.fat.zip")

	def test_v3_1_mac(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "darwin",
			version = "3.1"),
			"https://downloads.tuxfamily.org/godotengine/3.1/Godot_v3.1-stable_osx.64.zip")

	def test_v3_2_mac(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "darwin",
			version = "3.2"),
			"https://downloads.tuxfamily.org/godotengine/3.2/Godot_v3.2-stable_osx.64.zip")

	def test_v3_3_mac(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "darwin",
			version = "3.3"),
			"https://downloads.tuxfamily.org/godotengine/3.3/Godot_v3.3-stable_osx.universal.zip")

	def test_v3_4_mac(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "darwin",
			version = "3.4"),
			"https://downloads.tuxfamily.org/godotengine/3.4/Godot_v3.4-stable_osx.universal.zip")

	def test_v3_5_mac(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "darwin",
			version = "3.5"),
			"https://downloads.tuxfamily.org/godotengine/3.5/Godot_v3.5-stable_osx.universal.zip")

	def test_v4_0_mac(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "darwin",
			version = "4.0"),
			"https://downloads.tuxfamily.org/godotengine/4.0/Godot_v4.0-stable_macos.universal.zip")

	def test_v4_1_mac(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "darwin",
			version = "4.1"),
			"https://downloads.tuxfamily.org/godotengine/4.1/Godot_v4.1-stable_macos.universal.zip")

	# test all stable linux versions

	def test_v1_0_linux(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "linux",
			version = "1.0"),
			"https://downloads.tuxfamily.org/godotengine/1.0/Godot_v1.0_stable_x11.64.zip")

	def test_v1_1_linux(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "linux",
			version = "1.1"),
			"https://downloads.tuxfamily.org/godotengine/1.1/Godot_v1.1_stable_x11.64.zip")

	def test_v2_0_linux(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "linux",
			version = "2.0"),
			"https://downloads.tuxfamily.org/godotengine/2.0/Godot_v2.0_stable_x11.64.zip")

	def test_v2_1_linux(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "linux",
			version = "2.1"),
			"https://downloads.tuxfamily.org/godotengine/2.1/Godot_v2.1-stable_x11.64.zip")

	def test_v3_0_linux(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "linux",
			version = "3.0"),
			"https://downloads.tuxfamily.org/godotengine/3.0/Godot_v3.0-stable_x11.64.zip")

	def test_v3_1_linux(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "linux",
			version = "3.1"),
			"https://downloads.tuxfamily.org/godotengine/3.1/Godot_v3.1-stable_x11.64.zip")

	def test_v3_2_linux(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "linux",
			version = "3.2"),
			"https://downloads.tuxfamily.org/godotengine/3.2/Godot_v3.2-stable_x11.64.zip")

	def test_v3_3_linux(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "linux",
			version = "3.3"),
			"https://downloads.tuxfamily.org/godotengine/3.3/Godot_v3.3-stable_x11.64.zip")

	def test_v3_4_linux(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "linux",
			version = "3.4"),
			"https://downloads.tuxfamily.org/godotengine/3.4/Godot_v3.4-stable_x11.64.zip")

	def test_v3_5_linux(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "linux",
			version = "3.5"),
			"https://downloads.tuxfamily.org/godotengine/3.5/Godot_v3.5-stable_x11.64.zip")

	def test_v4_0_linux(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "linux",
			version = "4.0"),
			"https://downloads.tuxfamily.org/godotengine/4.0/Godot_v4.0-stable_linux.x86_64.zip")

	def test_v4_1_linux(self):
		self.assertEqual(godot_download.get_download_url(
			use_tux = True,
			system = "linux",
			version = "4.1"),
			"https://downloads.tuxfamily.org/godotengine/4.1/Godot_v4.1-stable_linux.x86_64.zip")
