A tool to assist you in managing your Godot projects.

## How To Install
Please see the [Installation Guide](https://gs-project-manager-docs.readthedocs.io/en/latest/install.html) guide in the [Documentation](https://gs-project-manager-docs.readthedocs.io/en/latest/index.html).

## How To Use
There is an example project [here](https://gs-project-manager-docs.readthedocs.io/en/latest/tappy.html) to get you started.

Below is an example that will create a new Godot project and show you how to use some basic commands.
```bash
# create a new project
> gspm new test-project
Project test-project has been created

# install assets
> cd test-project 
> gspm install

# edit your project
> gspm edit 
```


