
"""
Program: Run GSPM From a CLI or IDE

Remarks:

    Useful when you do not want to actually
    install the tool into the Virtual
    Environment when testing.
"""

from gspm.gspm import run

if __name__ == '__main__':
    run()
