For a complete list of changes, please see [HISTORY.md] file.

## v0.1.11 Release Notes

### Changes
* added godot version 3.1.1
* edit project outside of project.yml location by specifying folder name on command
    ```
    > gspm edit my-project-folder
    ```
### Fixes
* install command failed in some instances
* fix godot 3.1 repository location