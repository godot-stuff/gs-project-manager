

from packaging.version import Version


def get_download_url(**kwargs):

	if 'system' not in kwargs:
		raise KeyError('missing system in download arguments')
	
	if 'version' not in kwargs:
		raise KeyError('missing version in download arguments')

	_system = kwargs.get('system', '')
	_version = kwargs.get('version', '')
	_release = kwargs.get('release', 'stable')
	_arch = kwargs.get('architecture', '64')
	_is_mono = kwargs.get('is_mono', False)
	_use_tux = kwargs.get('use_tux', False)

	if _use_tux:
		url = get_tux_url(_system, _version, _release, _arch, _is_mono)
		print(url)
		return url
	else:
		url = get_git_url(_system, _version, _release, _arch, _is_mono)
		print(url)
		return url


def get_git_url(system: str, version: str, release: str, architecture: str, is_mono: bool):


	v_version = version
	v_release = release
	v_system = system
	v_arch = architecture
	v_zip = ".zip"

	v_url = 'https://github.com/godotengine/godot-builds/releases/download'

	uri = ''

	if len(v_release) == 0:
		v_release = 'stable'

	if v_system == "windows":

		v_system = "win"

		if is_mono:
			v_system = "mono_win"
			v_zip = ".zip"
		else:
			v_zip = '.exe.zip'


	if v_system == "darwin":

		v_system = "macos."
		v_arch = "universal"

		if Version(v_version) < Version("3.6"):
			v_system = "osx."

		if Version(v_version) < Version("3.3"):
			v_arch = "64"

		if Version(v_version) < Version("3.1"):
			v_arch = "fat"

		if Version(v_version) < Version("2.1"):
			v_system = 'osx'
			v_arch = "32"

	if v_system == 'linux':

		v_system = 'linux.x86_'

		if Version(v_version) < Version("4.0"):
			v_system = 'x11.'

	uri = f"{v_url}/{v_version}-{v_release}/Godot_v{v_version}-{v_release}_{v_system}{v_arch}{v_zip}"

	# < 2.1

	if Version(v_version) < Version("2.1"):

		uri = f"{v_url}/{v_version}-{v_release}/Godot_v{v_version}_{v_release}_{v_system}{v_arch}{v_zip}"

	return uri


def get_tux_url(system: str, version: str, release: str = "", architecture: str = "64", is_mono: bool = False):

	v_version = version
	v_release = release
	v_system = system
	v_arch = architecture

	v_url = 'https://downloads.tuxfamily.org/godotengine'

	uri = ""

	# if is_mono:
	# 	v_mono_folder = "mono/"
	# 	v_mono_filename = "mono_"

	if v_release == 'stable':
		v_release = None

	if v_system == "windows":

		v_system = "win"

		# default

		uri = f"{v_url}/{v_version}/Godot_v{v_version}-stable_{v_system}{v_arch}.exe.zip"

		if is_mono:
			uri = f"{v_url}/{v_version}/mono/Godot_v{v_version}-stable_mono_{v_system}{v_arch}.zip"

		if v_release:
			uri = f"{v_url}/{v_version}/{v_release}/Godot_v{v_version}-{v_release}_{v_system}{v_arch}.exe.zip"

			if is_mono:
				uri = f"{v_url}/{v_version}/{v_release}/mono/Godot_v{v_version}-{v_release}_mono_{v_system}{v_arch}.zip"

		# < 2.1

		if Version(v_version) < Version("2.1"):

			uri = f"{v_url}/{v_version}/Godot_v{v_version}_stable_{v_system}{v_arch}.exe.zip"

			if is_mono:
				uri = f"{v_url}/{v_version}/mono/Godot_v{v_version}_stable_mono_{v_system}{v_arch}.zip"

			if v_release:
				uri = f"{v_url}/{v_version}/{v_release}/Godot_v{v_version}_{v_release}_{v_system}{v_arch}.exe.zip"

				if is_mono:
					uri = f"{v_url}/{v_version}/{v_release}/mono/Godot_v{v_version}_{v_release}_mono_{v_system}{v_arch}.zip"


	if v_system == "linux":

		v_system = "linux.x86"

		# default

		uri = f"{v_url}/{v_version}/Godot_v{v_version}-stable_{v_system}_{v_arch}.zip"

		if is_mono:
			v_system = "linux_x86"
			uri = f"{v_url}/{v_version}/mono/Godot_v{v_version}-stable_mono_{v_system}_{v_arch}.zip"

		if v_release:
			uri = f"{v_url}/{v_version}/{v_release}/Godot_v{v_version}-{v_release}_{v_system}_{v_arch}.zip"

			if is_mono:
				v_system = "linux_x86"
				uri = f"{v_url}/{v_version}/{v_release}/mono/Godot_v{v_version}-{v_release}_mono_{v_system}_{v_arch}.zip"

		# < 4.0

		if Version(v_version) < Version("4.0"):

			v_system = "x11"

			uri = f"{v_url}/{v_version}/Godot_v{v_version}-stable_{v_system}.{v_arch}.zip"

			if is_mono:
				uri = f"{v_url}/{v_version}/mono/Godot_v{v_version}-stable_mono_{v_system}_{v_arch}.zip"

			if v_release:
				uri = f"{v_url}/{v_version}/{v_release}/Godot_v{v_version}-{v_release}_{v_system}.{v_arch}.zip"

				if is_mono:
					uri = f"{v_url}/{v_version}/{v_release}/mono/Godot_v{v_version}-{v_release}_mono_{v_system}_{v_arch}.zip"

		# < 2.1

		if Version(v_version) < Version("2.1"):

			uri = f"{v_url}/{v_version}/Godot_v{v_version}_stable_{v_system}.{v_arch}.zip"

			if is_mono:
				uri = f"{v_url}/{v_version}/mono/Godot_v{v_version}_stable_mono_{v_system}.{v_arch}.zip"

			if v_release:
				uri = f"{v_url}/{v_version}/{v_release}/Godot_v{v_version}_{v_release}_{v_system}.{v_arch}.zip"

				if is_mono:
					uri = f"{v_url}/{v_version}/{v_release}/mono/Godot_v{v_version}_{v_release}_mono_{v_system}.{v_arch}.zip"


	if v_system == "darwin":

		v_system = "macos"
		v_arch = "universal"

		# default

		if Version(v_version) < Version("3.6"):
			v_system = "osx"

		if Version(v_version) < Version("3.3"):
			v_arch = "64"

		if Version(v_version) < Version("3.1"):
			v_arch = "fat"

		uri = f"{v_url}/{v_version}/Godot_v{v_version}-stable_{v_system}.{v_arch}.zip"

		if is_mono:
			uri = f"{v_url}/{v_version}/mono/Godot_v{v_version}-stable_mono_{v_system}.{v_arch}.zip"

		if v_release:
			uri = f"{v_url}/{v_version}/{v_release}/Godot_v{v_version}-{v_release}_{v_system}.{v_arch}.zip"

			if is_mono:
				uri = f"{v_url}/{v_version}/{v_release}/mono/Godot_v{v_version}-{v_release}_mono_{v_system}.{v_arch}.zip"

		# < 2.1

		if Version(v_version) < Version("2.1"):
			v_arch = "32"

			if Version(v_version) > Version("2.0.3"):
				v_arch = ".fat"

			uri = f"{v_url}/{v_version}/Godot_v{v_version}_stable_{v_system}{v_arch}.zip"

			if is_mono:
				uri = f"{v_url}/{v_version}/mono/Godot_v{v_version}_stable_mono_{v_system}{v_arch}.zip"

			if v_release:
				uri = f"{v_url}/{v_version}/{v_release}/Godot_v{v_version}_{v_release}_{v_system}{v_arch}.zip"

				if is_mono:
					uri = f"{v_url}/{v_version}/{v_release}/mono/Godot_v{v_version}_{v_release}_mono_{v_system}{v_arch}.zip"

	return uri
