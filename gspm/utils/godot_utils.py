
import logging
import platform
import urllib.request

import wget
import zipfile
import os
import gspm.utils.process_utils as process_utils
import subprocess
import requests
import sys
from urllib.parse import urlparse
import gspm.utils.godot_download as godot_download
import gspm.utils.path_utils as path_utils

from packaging.version import Version
from gspm.utils.godot_download import *


host_url = "https://downloads.tuxfamily.org/godotengine/{0}/"


def edit_godot(project):

	cmd = _build_godot_cmd(project)
	cmd = "{0} -e".format(cmd)
	# cmd = "start dir"
	logging.debug("- running command {0}".format(cmd))
	process_utils.run_process(cmd, True)


def install_godot(project):

	logging.debug("[godot_utils] install_godot")

	if project.config.godot.local:
		pass
	else:
		# if Version('{0}'.format(project.config.godot.version)) < Version('2.1'):
		#     raise Exception("Version [{0}] Not Supported".format(project.config.godot.version))

		# if Version('{0}'.format(project.config.godot.version)) > Version('3.2.3'):
		#     raise Exception("Version [{0}] Not Supported".format(project.config.godot.version))

		logging.debug(
			"- checking for godot [{0}]".format(project.config.godot.version))

		dest_path = \
			"{0}/godot-{1}" \
			.format(project.repository_home, project.config.godot.version)

		dest_path = os.path.abspath(dest_path)

		if os.path.exists(dest_path):
			if not project.args.force:
				logging.info(
					"- godot is already available at [{0}] - skipping"
					.format(dest_path))
				return
			else:
				path_utils.clean_path(dest_path)

		path_utils.create_path(dest_path)

		_url = _get_godot_url(project)

		_mono = ''
		if (project.config.godot.mono):
			_mono = ' with mono'

		_release = "stable"
		if (project.config.godot.release):
			_release = project.config.godot.release

		_version = "Version {0} {1} ({2}){3} for {4}".format(project.config.godot.version, _release, project.config.godot.arch, _mono, _get_platform())

		project.config.uri = _url

		logging.log(99,
			"looking for godot {0}".format(_version))

		# get = requests.get(_url)

		code = urllib.request.urlopen(_url).code

		# if get.status_code != 200:
		if code != 200:
			raise Exception("{0} Not Available.".format(_version))

		try:

			logging.info(f"found version {_version}")
			logging.debug("- downloading [{0}] to [{1}]".format(_url, dest_path))
			file = os.path.join(dest_path, "file.zip")
			download_to_file(_url, file)
			logging.log(99, "\r")
			logging.info("\r- dowload complete")
			zipf = zipfile.ZipFile(file)
			zipf.extractall(dest_path)
			zipf.close()

			if _get_platform() == "darwin":
				subprocess.call(['chmod', '-R', '+x', dest_path])
				
			if _get_platform() == "linux":
				subprocess.call(['chmod', '-R', '+x', dest_path])

			os.remove(file)

		except Exception as e:

			print(e)
			sys.exit(0)

			raise Exception("Could not download {0}. Please check the version you want and try again.".format(_version))


def run_godot(project):
	cmd = _build_godot_cmd(project)
	cmd = "{0} -r".format(cmd)
	# cmd = "start dir"
	logging.debug("- running command {0}".format(cmd))
	return process_utils.run_process(cmd, True)


def run_godot_script(project, script):
	cmd = _build_godot_cmd(project)
	cmd = "{0} {1}".format(cmd, script)
	# cmd = "start dir"
	logging.debug("- running command {0}".format(cmd))
	return process_utils.run_process(cmd, True)


def export_godot(project, name, path):
	cmd = _build_godot_cmd(project)
	cmd = "{0} --no-window --export {1} {2}".format(cmd, name, os.path.abspath(path))
	# cmd = "start dir"
	logging.debug("- running command {0}".format(cmd))
	return process_utils.run_process(cmd, True)


def _get_godot_url(project):
	logging.debug("[godot_utils] _get_godot_url")

	_system = _get_platform()
	_url = godot_download.get_download_url(
		system = _system,
		version = str(project.config.godot.version),
		release = str(project.config.godot.release),
		architecture = str(project.config.godot.arch),
		is_mono = project.config.godot.mono,
		use_tux=False)
	return _url


def _build_godot_cmd(project):
	logging.debug("[godot_utils] _build_godot_cmd")
	system = _get_platform()
	cmd = ""

	if system == "windows":
		cmd = _build_windows_cmd(project)

	if system == "darwin":
		cmd = _build_darwin_cmd(project)

	if system == "linux":
		cmd = _build_linux_cmd(project)

	if not cmd:
		raise Exception("Platform [{0}] Not Supported".format(system))

	return cmd


def _build_windows_cmd(project):
	
	logging.debug("[godot_utils] _build_windows_cmd")
	cmd = ""
	proj_path = os.path.abspath(project.project_path)

	if project.config.godot.local:
		cmd = 'start "" "{0}" --path "{1}"'.format(os.path.abspath(project.config.godot.local), proj_path)
	else:
		godot_path = os.path.abspath("{0}/godot-{1}".format(project.repository_home, project.config.godot.version))
		cmd = 'start "" "{0}\{1}" --path "{2}"'.format(godot_path, _get_godot_runtime(project), proj_path)

	return cmd


def _build_linux_cmd(project):

	cmd = ""
	proj_path = os.path.abspath(project.project_path)

	if project.config.godot.local:
		cmd = "{0} --path {1}".format(os.path.abspath(project.config.godot.local), proj_path)
	else:
		godot_path = os.path.abspath(f"{project.repository_home}/godot-{project.config.godot.version}")
		proj_path = os.path.abspath(project.project_path)
		cmd = f"{godot_path}/{_get_godot_runtime(project)} --path {proj_path}"

	return cmd


def _build_darwin_cmd(project):

	cmd = ""
	proj_path = os.path.abspath(project.project_path)

	if project.config.godot.location:
		cmd = "{0} --path {1}".format(os.path.abspath(project.config.godot.location), proj_path)
	else:
		godot_path = os.path.abspath("{0}/godot-{1}".format(project.repository_home, project.config.godot.version))
		cmd = "arch -{3} {0}/{1} --path {2}".format(godot_path, _get_godot_runtime(project), proj_path, project.config.godot.arch)
		logging.debug(cmd)

	return cmd


#   return the platform we are running on
def _get_platform():
	logging.debug('[godot_utils] _get_platform')
	return platform.system().lower()


def _get_godot_runtime(project):
	logging.debug('[godot_utils] _get_godot_runtime')
	plat = _get_platform()
	runtime = ""

	if plat == "windows":
		runtime = _get_windows_runtime(project)

	if plat == "darwin":
		runtime = _get_darwin_runtime(project)

	if plat == "linux":
		runtime = _get_linux_runtime(project)

	if not runtime:
		raise Exception("Platform [{0}] Not Supported".format(plat))

	return runtime


def _get_windows_runtime(project):
	logging.debug('[godot_utils] _get_windows_runtime')
	_uri = _get_godot_url(project)
	_runtime = _uri[_uri.rfind("/")+1:]
	_runtime = _runtime.replace('.zip', '')

	if project.args.console:
		_runtime = _runtime.replace('.exe', '_console.exe')

	if (project.config.godot.mono):
		_runtime = '{0}\{0}'.format(_runtime)
		
	return _runtime


def _get_linux_runtime(project):
	logging.debug('[godot_utils] _get_linux_runtime')
	_uri = _get_godot_url(project)
	_runtime = _uri[_uri.rfind("/")+1:]
	_runtime = _runtime.replace('.zip', '')

	if (project.config.godot.mono):
		_runtime = '{0}/{0}'.format(_runtime)
		_s = list(_runtime)
		_s[_runtime.rfind("_")] = '.'
		_runtime = "".join(_s)

	return _runtime


def _get_darwin_runtime(project):
	logging.debug('[godot_utils] _get_darwin_runtime')
	runtime = "Godot.app/Contents/MacOS/Godot"
	if (project.config.godot.mono):
			runtime = "Godot_mono.app/Contents/MacOS/Godot"

	return runtime


def download_to_file(url, filename):

	logging.debug("[godot_utils] download_to_file")

	with open(filename, 'wb') as f:

		response = requests.get(url, stream=True, timeout=5.0)
		total = response.headers.get('content-length')

		if total is None:
			f.write(response.content)
		else:
			downloaded = 0
			total = int(total)
			for data in response.iter_content(chunk_size=max(int(total/1000), 1024*1024)):
				downloaded += len(data)
				f.write(data)
				done = int(50*downloaded/total)
				sys.stdout.write('\r[{}{}]'.format('█' * done, '.' * (50-done)))
				sys.stdout.flush()
	
	sys.stdout.write('\n')