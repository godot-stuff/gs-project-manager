import io

__logo__ = """
   __ _ ___ _ __  _ __ ___  
  / _` / __| '_ \| '_ ` _ \ 
 | (_| \__ \ |_) | | | | | |
  \__, |___/ .__/|_| |_| |_|
  |___/    |_|              

"""


__id__ = "gspm"
__name__ = "godot-stuff Project Manager"
__desc__ = "A Command Line Utility to Assist in Managing your GODOT projects."
__copyright__ = "Copyright 2018-2021, SpockerDotNet LLC"
__version__ = '0.1.37'
__url__ = "https://pypi.org/project/gspm/"
__project__ = "https://gitlab.com/retro-coder/commodore/kick-assembler-vscode-ext"
__docs__ = "https://gs-project-manager-docs.readthedocs.io/en/latest/index.html"
