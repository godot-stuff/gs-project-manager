
# How To Contribute

## Publishing
```bash
# make sure you have the following packages
#   
#   - twine
#   - setuptools
#   - wheel
#

# create distro
> python setup.py sdist

# upload to pypi using twine
> twine upload dist/*
```